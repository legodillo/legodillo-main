/*
 * turret_thread.h
 *
 *  Created on: Jan 26, 2013
 *      Author: akonradi
 */

#ifndef TURRET_THREAD_H_
#define TURRET_THREAD_H_

#define TURRET_SERVO_LAUNCHER_LAUNCH 325
#define TURRET_SERVO_LAUNCHER_NORMAL 247
#define TURRET_SERVO_LAUNCHER_LOAD 175

#define TURRET_THREAD_PAUSE_TIME 100

void turretAimFuture(struct location target, struct location robotFutureLocation, float robotFutureHeading);

void turretLauncherStop();

bool getTurretReady();

void turretFire();

void turretLauncherPause();

void turretLauncherResume();
#endif /* TURRET_THREAD_H_ */
