
 #include <joyos.h>
 #include "location.h"

 // Start numbering at positive x-axis, and move clockwise
 const struct location INNER_SAFE_NODES[] = {
       {  .x = 56.4f, .y = 32.93f},
         {  .x = -2.16f, .y = 60.5f},
         {  .x = -58.58f, .y = 29.08f},
         {  .x = -46.91f, .y = -31.49f},
         {  .x = -.89f, .y = -57.63f},
         {  .x = 48.34f, .y = -27.27f}
 };
 const struct location BOARD_CENTER = {.x = 0, .y = 0};

 const struct location ZONE_RESOURCE_POINT[]= {
	{	.x = 87.6f, .y = -15.7f}, //done
 	{	.x = 59.5f, .y = 70.9f},//done
 	{	.x = -29.5, .y = 85.30f},//done
 	{	.x = -88.5f, .y = 16.7f}, //done
 	{	.x = -61.f, .y = -67.5}, //done
 	{	.x = 26.8f, .y = -84.8f} //done
 };
 const float ZONE_RESOURCE_HEADING[] = {
 		-31.8, 28.00f, 91.3f, 148.f, -150.f, -95.5f
 };

 const float ZONE_CAPTURE_HEADING[] = {
 		-59, 4.f, 64.3f, 119.90f, -179.f, -119.f
 };

 const struct location ZONE_CAPTURE_POINT[]= {
 	{	.x = 80.f, .y = 19.f}, //done
 	{	.x =26.5f, .y = 82.f}, //done
	{	.x = -56.5f, .y = 62.7f},  //done
 	{	.x = -82.5f, .y = -15.5f},  //done
	{	.x = -28.2f, .y = -77.2f}, //done
 	{	.x = 54.f, .y = -60.60f } //done
 };


const struct location RED_SCORE_TARGET_NEAR = { .x = 4.82f, .y = 11.19f }; //done
const struct location BLUE_SCORE_TARGET_NEAR = { .x = -8.16f, .y = -10.66f };  //done

const struct location RED_SCORE_TARGET_FAR = { .x = 5.784f, .y = 13.43f }; //done
const struct location BLUE_SCORE_TARGET_FAR = { .x = -9.79f, .y = -12.79f };  //done
