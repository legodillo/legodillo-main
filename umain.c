#include <stdio.h>
#include <motor.h>
#include <buttons.h>
#include <inttypes.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include <math.h>
#include <joyos.h>
#include "legodillo.h"
#include "movement.h"
#include "strategy_engine.h"
#include "location.h"
#include "turret_thread.h"

#define SWITCH_PORT 7

#define CAPTURE_SERVO_PORT 2
#define CAPTURE_SERVO_IN 420
#define CAPTURE_SERVO_OUT 171

#define MINE_SERVO_FULLY_UP 508
#define MINE_SERVO_UP 185
#define MINE_SERVO_DOWN 86

bool capturePoint();
uint8_t minePoint();

bool CURRENT_COLOR;

extern bool captureFlag;
extern bool mineFlag; //tell umain whether destination is gear box or ball pipe.

extern struct location INNER_SAFE_NODES[];
extern struct location ZONE_CAPTURE_POINT[];
extern float ZONE_CAPTURE_HEADING[];
extern struct location ZONE_RESOURCE_POINT[];
extern float ZONE_RESOURCE_HEADING[];
extern struct location BLUE_SCORE_TARGET_FAR;
extern struct location BLUE_SCORE_TARGET_NEAR;

extern struct location RED_SCORE_TARGET_FAR;
extern struct location RED_SCORE_TARGET_NEAR;

extern struct location BOARD_CENTER;

void usetup(void) {
	extern volatile uint8_t robot_id;
	robot_id = TEAM_LEGODILLO;
	gyro_init(GYRO_PORT, LSB_US_PER_DEG, 500L);
}

extern struct location currentLocation;

void umain(int argc, char * argv[]) {
	strategy_engine_start();
	currentLocation = (struct location ) { .x = -80, .y = 0 };
	setLocationFromVPS();
	tenSeconds();
	printf("ten seconds done\n");

	while (1) {
		printf("enter main loop\n");
		bool captureTarget;
		printf("call gameStrategy\n");
		int targetZone = gameStrategy(&captureTarget);
		printf("I HAZ A DESTINASHUN\n");
		struct location newDestination;
		if (captureTarget) {
			newDestination = ZONE_CAPTURE_POINT[targetZone];
		} else {
			newDestination = ZONE_RESOURCE_POINT[targetZone];
			struct location target;
			if (targetZone == 3 || targetZone == 4 || targetZone == 5) {
				if (CURRENT_COLOR == RED)
					target = RED_SCORE_TARGET_FAR;
				else
					target = BLUE_SCORE_TARGET_NEAR;
			} else {
				if (CURRENT_COLOR == RED)
					target = RED_SCORE_TARGET_NEAR;
				else
					target = BLUE_SCORE_TARGET_FAR;
			}
			turretAimFuture(target, newDestination, captureTarget ? ZONE_CAPTURE_HEADING[targetZone] : ZONE_RESOURCE_HEADING[targetZone]);
		}

		int8_t currentZone = getZone(getCurrentLocation());
		int8_t destinationZone = targetZone;
		bool direction = PATH_COUNTERCLOCKWISE;
		if (currentZone > destinationZone && currentZone - destinationZone < 3)
			direction = PATH_CLOCKWISE;
		else if (destinationZone > currentZone && destinationZone - currentZone > 3)
			direction = PATH_CLOCKWISE;
		if (direction == PATH_COUNTERCLOCKWISE) {
			uint8_t goToFirst = currentZone;
			for (uint8_t i = goToFirst; i != destinationZone; i = (i + 1) % 6) {
				goToPoint(INNER_SAFE_NODES[i]);
			}
		} else {
			uint8_t goToFirst = (currentZone - 1 + 6) % 6;
			for (uint8_t i = goToFirst; i != (destinationZone - 1 + 6) % 6; i = (i - 1 + 6) % 6) {
				goToPoint(INNER_SAFE_NODES[i]);
			}
		}
		goToPoint(getOffsetLocation(newDestination, getHeadingToLocation(newDestination, BOARD_CENTER), 6));
		pause(300);
		setLocationFromVPS();
		goToPoint(getOffsetLocation(newDestination, getHeadingToLocation(BOARD_CENTER, newDestination), 12));

		bool success;
		if (captureTarget) {
			success = turnToHeading(ZONE_CAPTURE_HEADING[targetZone]);
			if (success)
				capturePoint();
		} else {
			success = turnToHeading(ZONE_RESOURCE_HEADING[targetZone]);
			if (success) {
				turretLauncherResume();
				minePoint();
				turretLauncherPause();
				pause(300);
				setVelocity(MOTOR_BOTH, -100);
				pause(400);
				brake(MOTOR_BOTH);
				pause(300);
				setLocationFromVPS();
			}
		}

		if (!success) {
			setVelocity(MOTOR_BOTH, -200);
			pause(200);
			brake(MOTOR_BOTH);
			pause(300);
			setLocationFromVPS();
		}
	}
}

bool capturePoint() {
//start spinning wheel
	/*
	 * The blue team spins the gearbox counterclockwise, red clockwise.
	 * A positive motor velocity causes the wheel to spin clockwise, so red
	 * get a negative velocity.
	 */
	int8_t servoDirection = (CURRENT_COLOR == BLUE) ? 1 : -1;

	int servoPos;
	int switchTriggerPoint = 0; // At what servo point the switch was triggered
	bool motorOff = true; // Whether the motor is currently off
	for (servoPos = CAPTURE_SERVO_IN; servoPos > CAPTURE_SERVO_OUT - 1; servoPos -= 3) {
		servo_set_pos(CAPTURE_SERVO_PORT, servoPos);
		pause(4);
		// If far enough away from the wheels (20 from normal), turn on the motor
		if (motorOff && servoPos <= CAPTURE_SERVO_IN - 20) {
			motor_set_vel(MOTOR_CAPTURE_PORT, 200 * servoDirection);
			motorOff = false;
		}
		// If the switch is pressed for the first time, record it
		if (digital_read(SWITCH_PORT) && switchTriggerPoint == 0) {
			switchTriggerPoint = servoPos;
		}
		// If the servo has progressed 20 ticks past the trigger point, stop the servo
		if (switchTriggerPoint - servoPos > 20) {
			break;
		}
	}
	uint8_t currentZone = getZone(getCurrentLocation());
	uint32_t captureStartTime = get_time();

// Spin the motor until the territory is ours, or until 3 seconds have passed
	printf("Capturing zone %u\n", currentZone);
	copy_objects();
	while ((game.territories[currentZone].owner != TEAM_LEGODILLO) && (get_time() - captureStartTime < 1500)) {
		copy_objects();
		yield();
	}

	for (; servoPos < CAPTURE_SERVO_IN + 1; servoPos += 3) {
		servo_set_pos(CAPTURE_SERVO_PORT, servoPos);
		pause(4);
		if (servoPos >= CAPTURE_SERVO_OUT + 30) {
			// Total eye candy: slow down the motor as it nears the chassis.
			int motorSpeed = map(servoPos, CAPTURE_SERVO_OUT + 30, CAPTURE_SERVO_IN,200 * servoDirection,0);
			motor_set_vel(MOTOR_CAPTURE_PORT, motorSpeed);
		}
	}
	motor_brake(MOTOR_CAPTURE_PORT);

	return (game.territories[currentZone].owner == TEAM_LEGODILLO);
}
////	gyro_set_degrees(90);
//	strategy_engine_start();
//	dprintf("setting destination\n");
////	movement_thread_start();
//
//	struct location destination = (struct location ) { .x = 30, .y = -80 };
//	while (true) {
//		float heading = getHeadingToLocation(currentLocation, destination);
//		if (abs(normalizeHeading(heading - gyro_get_degrees())) < 5)
//			turnTowardsHeading(heading);
//		else if (distancetoPoint(currentLocation, destination) > 10)
//			moveTowardsPoint(destination);
//		else {
//			motor_brake(0);
//			motor_brake(1);
//		}
//	}

/** *while(1){
 *	struct location destPoint = gameStrategy();
 *	movementSetDestination(destPoint);
 *	if(captureFlag == true){
 *		if(arrivedAtDestination(destPoint) == true){ //i.e., arrived at capture destination
 *			capturePoint();
 *		}
 *	}
 *	else{ //mineFlag == true
 *		if(arrivedAtDestination(destPoint) == true){ //i.e., arrived at mining destination
 *			minePoint();
 *	}
 *}
 */
/*movementStop();
 while (true) {
 capturePoint();
 pause(1000);
 }
 */
/**
 * Returns whether the current zone has been captured
 **/
//
uint8_t minePoint() {
	uint8_t currentTerritory = getZone(getCurrentLocation());
	uint8_t startBallCount = game.territories[currentTerritory].remaining;

	for (uint8_t mineCount = 0; startBallCount - game.territories[currentTerritory].remaining < 5; mineCount++) {
		// careful that we don't get stuck in an infinite loop--need timeout! (after 15 seconds of mining, break)
		servo_set_pos(SERVO_MINE_PORT, MINE_SERVO_DOWN);
		pause(300);
		servo_set_pos(SERVO_MINE_PORT, MINE_SERVO_UP);
		pause(300);
		copy_objects();
		// If no balls have been mined and this is the 4th attempt, give up
		if ((startBallCount - game.territories[currentTerritory].remaining) + 4 <= mineCount)
			break;
	}
	servo_set_pos(SERVO_MINE_PORT, MINE_SERVO_FULLY_UP);
	pause(500);
	copy_objects();
	return game.territories[currentTerritory].remaining - startBallCount;
}
//
