#include <joyos.h>
#include <math.h>
#include "legodillo.h"
#include "location.h"
#include "movement.h"
#include "turret_thread.h"

#define TURRET_RPS_TOLERANCE .5
#define TURRET_HEADING_TOLERANCE 2.0

float turretTargetRelativeHeading;

float shootFromRobotHeading;
struct location shootFromRobotLocation;
struct location shootAtTarget;

int encoderTicksFromCenter;
uint16_t lastHeadingEncoder, lastLauncherEncoderVal;
uint32_t lastTime;
float launcherMotorVel;
volatile enum {
	CALCULATING, AIMING, FIRING, WAITING_FOR_BALL, WAITING_FOR_MORE_BALLS, INACTIVE, STOPPING, PAUSED
} turretState = INACTIVE;
int turretMove();

volatile bool turretPaused;

float getNecessaryRPS(float distance);

/**
 * Move the turret to aim towards a new point
 * This routine will start a new thread to move the turret. The new thread
 * will exit as soon as the turret is in position.
 *
 * @param target    			Where to shoot at
 * @param robotFutureLocation 	Where the robot will be when shooting
 * @param robotFutureHeading	What heading the robot will have when shooting
 *
 */
void turretAimFuture(struct location target, struct location robotFutureLocation, float robotFutureHeading) {
	shootFromRobotHeading = robotFutureHeading;
	shootFromRobotLocation = robotFutureLocation;
	shootAtTarget = target;
	if (turretState == INACTIVE) {
		create_thread(&turretMove, 10, 254, "turret_thread");
	} else
		turretState = CALCULATING;
}

void turretLauncherStop() {
	motor_brake(MOTOR_LAUNCHER_PORT);
	turretState = STOP;
}

void turretLauncherPause() {
	turretPaused = true;
}

void turretLauncherResume() {
	turretPaused = false;
}

/**
 * Thread to move the turret to aim at a new target
 * This thread will run until the turret is positioned correctly, then exit
 */
int turretMove() {
	printf("starting turret thread\n");
	turretState = CALCULATING;
	uint32_t lastBallTime = 0;
	struct location turretCenter;
	uint32_t lastTargetRPSReached = 0;
	bool turretHeadingDone = false;
	bool turretRPSDone = false;
	float launcherTargetRPS = 0;
	float targetRelativeAngle = 0;
	while (true) {
//		switch (turretState) {
//		case AIMING:
//			printf("aiming\n");
//			break;
//		case FIRING:
//			printf("firing\n");
//			break;
//		case INACTIVE:
//			printf("inactive\n");
//			break;
//		case WAITING_FOR_BALL:
//			printf("first wait\n");
//			break;
//		case WAITING_FOR_MORE_BALLS:
//			printf("nth wait\n");
//			break;
//		case CALCULATING:
//			printf("calculating\n");
//			break;
//		case STOPPING:
//			printf("stopping\n");
//			break;
//		}
		switch (turretState) {
		case CALCULATING:
			// Find the center (rotation point) of the turret, given the robot's location and heading
			turretCenter = getOffsetLocation(shootFromRobotLocation, shootFromRobotHeading - 180, TURRET_DIST_FROM_CENTER);

			// Calculate the heading to the target from the turret center
			float headingToTarget = getHeadingToLocation(turretCenter, shootAtTarget);

			// Calculate the actual point of launch for a ping-pong ball
			struct location turretLaunchPoint = getOffsetLocation(turretCenter, headingToTarget, TURRET_DIST_SWIVEL_TO_LAUNCH);

			// Calculate the direction the turret must face, relative to straight back
			targetRelativeAngle = normalizeHeading(headingToTarget - (shootFromRobotHeading + 180.0));

			// Calculate the target RPS from physics (Yay! Physics works!)
			launcherTargetRPS = getNecessaryRPS(distancetoPoint(turretLaunchPoint, shootAtTarget));
			dprintf("Target distance %6.2f\n", distancetoPoint(turretLaunchPoint, shootAtTarget));
			bool turretTurningPositive;
			encoder_reset(ENCODER_LAUNCHER_PORT);

			turretHeadingDone = false;
			turretRPSDone = false;
			lastTargetRPSReached = 0;
			turretState = AIMING;
			break;
		case AIMING:
			// Heading stuff
			// Read the heading encoder
			if (!turretHeadingDone) {
				float deltaHeading = targetRelativeAngle - encoderTicksFromCenter * TURRET_HEADING_ENCODER_TO_DEGREES;
				turretTurningPositive = deltaHeading > 0;
				uint16_t headingEncoderVal = encoder_read(ENCODER_TURRET_PORT);
				encoderTicksFromCenter += (turretTurningPositive ? 1 : -1) * (headingEncoderVal - lastHeadingEncoder);
				dprintf(" delta Heading %6.2f\n", deltaHeading);
				if (abs(deltaHeading) >= TURRET_HEADING_TOLERANCE) {
					float motorOutput = sign(deltaHeading) * constrain( map(abs(deltaHeading)*.5,0,180,170,200),0,200);
					// Motor is reversed
					motor_set_vel(MOTOR_TURRET_PORT, -motorOutput);
				} else {
					motor_brake(MOTOR_TURRET_PORT);
					turretHeadingDone = true;
					dprintf("Turret heading is ready\n");
				}
				lastHeadingEncoder = headingEncoderVal;
			}
			// RPS stuff

			uint32_t currentTime = get_time();
			uint16_t currentLauncherEncoderVal = encoder_read(ENCODER_LAUNCHER_PORT);
			float encoderRPS = ((float) (currentLauncherEncoderVal - lastLauncherEncoderVal) / (currentTime - lastTime)) * 1000.;
			float wheelRPS = encoderRPS / TURRET_LAUNCHER_ENCODER_RATIO; //convert to wheel rotations per second
			float launcherDeltaRPS = launcherTargetRPS - wheelRPS;
			dprintf("RPS: target %6.2f actual %6.2f delta %6.2f %6.2f\n", launcherTargetRPS, wheelRPS, launcherDeltaRPS, launcherMotorVel);
			if (launcherTargetRPS == 0) {
				launcherMotorVel = 0;
				motor_set_vel(MOTOR_LAUNCHER_PORT, -launcherMotorVel);
			}
			if (abs(launcherDeltaRPS) < TURRET_RPS_TOLERANCE) {
				if (!turretRPSDone) {
					lastTargetRPSReached = get_time();
				}
				turretRPSDone = true;
				dprintf("Turret RPS is ready\n");
			} else {
				turretRPSDone = false;
				launcherMotorVel += launcherDeltaRPS * 2;
				launcherMotorVel = constrain(launcherMotorVel, 0, 255);
				motor_set_vel(MOTOR_LAUNCHER_PORT, (int) -launcherMotorVel); // The motor spins backwards for positive values
			}

			lastLauncherEncoderVal = currentLauncherEncoderVal;
			lastTime = currentTime;

			if (turretRPSDone && turretHeadingDone && get_time() - lastTargetRPSReached > 1500) {
				turretState = WAITING_FOR_BALL;
			}
			pause(50);

			break;
		case WAITING_FOR_BALL:
			lastBallTime = 0;
			if (analog_read(BREAKBEAM_SENSOR) > 200) {
				turretState = FIRING;
			} else {
				pause(50);
			}
			break;
		case WAITING_FOR_MORE_BALLS:
			if (analog_read(BREAKBEAM_SENSOR) > 200) {
				turretState = FIRING;
			} else {
				if (lastBallTime == 0) {
					lastBallTime = get_time();
				}
			}
			if (lastBallTime != 0 && (get_time() - lastBallTime) > 2000) {
				turretState = STOPPING;
			}
			break;
		case FIRING:
			if (turretPaused)
				break;
			turretFire();
			turretState = WAITING_FOR_MORE_BALLS;
			lastBallTime = 0;
			break;
		case STOPPING:
			motor_set_vel(MOTOR_TURRET_PORT, 0);
			motor_set_vel(MOTOR_LAUNCHER_PORT, 0);
			turretState = INACTIVE;
			turretPaused = false;
			break;
		case INACTIVE:
			return 0;
		}
	}
	return 0;
}

float getNecessaryRPS(float distToBallZone) {
	float anglerad = (THETA * (PI / 180.0));
	float numerator = (distToBallZone * ERROR_FACTOR / 100.0 * G);
	float denominator = (pow((WHEEL_RADIUS), 2) * sin(2 * anglerad)
			- (1 / (2 * BALL_MASS)) * DRAG_COEFF * RHO_AIR * PI * pow((BALL_RADIUS), 2) * pow((WHEEL_RADIUS), 2) * distToBallZone
					* ERROR_FACTOR / 100.0);
	float coeff = (1.0 / (2.0 * PI));
	float RPS = (coeff * sqrt((numerator) / (denominator)));
	return RPS;
}

bool getTurretReady() {
	return turretState == WAITING_FOR_BALL || turretState == WAITING_FOR_MORE_BALLS;
}

void turretFire() {
	servo_set_pos(SERVO_LAUNCHER_PORT, TURRET_SERVO_LAUNCHER_LOAD);
	pause(250);
	servo_set_pos(SERVO_LAUNCHER_PORT, TURRET_SERVO_LAUNCHER_LAUNCH);
	pause(250);
	servo_set_pos(SERVO_LAUNCHER_PORT, TURRET_SERVO_LAUNCHER_NORMAL);
	pause(250);
}
