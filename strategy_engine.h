#ifndef STRATEGY_ENGINE_H
#define STRATEGY_ENGINE_H
#include <math.h>
//#include <joyos.h>
#include "location.h"

#define RED false
#define BLUE true

bool captureFlag; //captureFlag and mineFlag tell umain what the destination point actually is.
bool mineFlag;

int getGameElapsedTime();
int getGameRemainingTime();
void strategy_engine_start();

void tenSeconds();

uint8_t gameStrategy(bool*);

#endif
