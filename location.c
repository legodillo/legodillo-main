#include <stdbool.h>
#include <joyos.h>
#include <math.h>
#include "legodillo.h"
#include "location.h"

float getHeadingToLocation(struct location from, struct location to) {
	return (180.0 / PI * atan2((to.y - from.y), (to.x - from.x)));
}

float distancetoPoint(struct location from, struct location to) { //returns distance from any given objective point
	return sqrt(pow((from.x - to.x), 2) + pow((from.y - to.y), 2));
}

// modified version of a "
//  public domain function by Darel Rex Finley, 2006
//  Determines the intersection point of the line segment defined by points A and B
//  with the line segment defined by points C and D.
//
//  Returns YES if the intersection point was found, and stores that point in X,Y.
//  Returns NO if there is no determinable intersection point, in which case X,Y will
//  be unmodified.
// "

bool lineSegmentIntersection(struct location A, struct location B, struct location C, struct location D, float* X, float* Y) {
	printf("lineSegmentInter\n");
	float distAB, theCos, theSin, newX, ABpos;

	//  Fail if either line segment is zero-length.
	if ((A.x == B.x && A.y == B.y) || (C.x == D.x && C.y == D.y))
		return false;

	//  Fail if the segments share an end-point.
	if ((A.x == C.x && A.y == C.y) || (B.x == C.x && B.y == C.y) || (A.x == D.x && A.y == D.y) || (B.x == D.x && B.y == D.y)) {
		return false;
	}

	//  (1) Translate the system so that point A is on the origin.
	B.x -= A.x;
	B.y -= A.y;
	C.x -= A.x;
	C.y -= A.y;
	D.x -= A.x;
	D.y -= A.y;

	//  Discover the length of segment A-B.
	distAB = sqrt(B.x * B.x + B.y * B.y);

	//  (2) Rotate the system so that point B is on the positive X axis.
	theCos = B.x / distAB;
	theSin = B.y / distAB;
	newX = C.x * theCos + C.y * theSin;
	C.y = C.y * theCos - C.x * theSin;
	C.x = newX;
	newX = D.x * theCos + D.y * theSin;
	D.y = D.y * theCos - D.x * theSin;
	D.x = newX;

	//  Fail if segment C-D doesn't cross line A-B.
	if ((C.y < 0. && D.y < 0.) || (C.y >= 0. && D.y >= 0.))
		return false;

	//  (3) Discover the position of the intersection point along line A-B.
	ABpos = D.x + (C.x - D.x) * D.y / (D.y - C.y);

	//  Fail if segment C-D crosses line A-B outside of segment A-B.
	if (ABpos < 0. || ABpos > distAB)
		return false;

	//  (4) Apply the discovered position to line A-B in the original coordinate system.
	*X = A.x + ABpos * theCos;
	*Y = A.y + ABpos * theSin;

	return true;
}

struct location getOffsetLocation(struct location input, float angle, float distance) {
	return (struct location ) { .x = input.x + cos(angle * PI / 180) * distance, .y = input.y + sin(angle * PI / 180) * distance } ;

		}
