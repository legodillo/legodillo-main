#include <joyos.h>
#include <math.h>
#include "strategy_engine.h"
#include "robot_physical_constants.h"
#include "legodillo.h"
#include "turret_thread.h"
#include "movement.h"
#include "location.h"

extern const struct location INNER_SAFE_NODES[6];

extern const struct location BOARD_CENTER;

extern const struct location ZONE_RESOURCE_POINT[6];

extern const struct location ZONE_CAPTURE_POINT[6];

volatile bool CURRENT_COLOR;

uint8_t minArrayIndex(float distanceArray[], uint8_t arrayLength);

bool zoneInArray(uint8_t zone, uint8_t zoneArray[], uint8_t arrayLength);

int gameStartTime;

int getGameRemainingTime() {
	return 120000 - (get_time() - gameStartTime);
}

int getGameElapsedTime() {
	return get_time() - gameStartTime;
}

void strategy_engine_start() {
// Wait until we have at least one VPS read
	copy_objects();
	// Determine whether we are red or blue this round
	CURRENT_COLOR = (game.coords[0].x > 0) ? BLUE : RED;
	printf("s_e: color: %s\n", CURRENT_COLOR == RED ? "RED" : "BLUE");
	gameStartTime = get_time();
}

void tenSeconds() { //strategy to loop in counter-clockwise circle around board for first ten seconds of the game
	int currentZone = getZone(getCurrentLocation());
	dprintf("zone: %u\n", currentZone);
	for (int i = 0; i < 6; i++) {
		struct location destinationPoint = INNER_SAFE_NODES[(i + currentZone) % 6];
		destinationPoint.x *= 1.3;
		destinationPoint.y *= 1.3;
		dprintf("go to zone %d: %6.2f %6.2f\n", i, destinationPoint.x, destinationPoint.y);
		goToPoint(destinationPoint);
		pause(300);
		setLocationFromVPS();
		if (getGameElapsedTime() >= 10000)
			break;
	}dprintf("ten seconds done\n");

}
/**
 * main game prioritization strategy. should return the next
 * point of interest (for the prioritization part)
 */
uint8_t gameStrategy(bool* capturePoint) {
	copy_objects();
	printf("IM IN UR GAMESTRATEGY\n");
	uint8_t* OWNED_AVAILABLE = malloc(sizeof(uint8_t) * 6);
	uint8_t* OWNED_UNAVAILABLE = malloc(sizeof(uint8_t) * 6);
	uint8_t* UNOWNED_AVAILABLE = malloc(sizeof(uint8_t) * 6);
	uint8_t* UNOWNED_UNAVAILABLE = malloc(sizeof(uint8_t) * 6);
	uint8_t oa = 0, ou = 0, ua = 0, uu = 0;
	printf("sorting zones\n");
	for (int zoneIndex = 0; zoneIndex < 6; zoneIndex++) {
		if (game.territories[zoneIndex].owner == TEAM_LEGODILLO) {
			if (game.territories[zoneIndex].rate_limit < 2) { //owned and available
				OWNED_AVAILABLE[oa] = zoneIndex;
				oa++;
			} else { //owned and unavailable
				OWNED_UNAVAILABLE[ou] = zoneIndex;
				ou++;
			}
		} else {
			if (game.territories[zoneIndex].rate_limit < 2) { //unowned and available
				UNOWNED_AVAILABLE[ua] = zoneIndex;
				ua++;
			} else { //unowned and unavailable
				UNOWNED_UNAVAILABLE[uu] = zoneIndex;
				uu++;
			}
		}
	}
	printf("OA: %d UA: %d OU: %d UU: %d\n", oa, ua, ou, uu);
//	printf("sorted\n");

	int currentZone = getZone(getCurrentLocation());
	/*
	 * categorization of all zones into one of the four characteristics now complete.
	 * number of each is given by oa, ou, ua, uu respectively.
	 * if any are owned and available, go there and mine, unless [unowned]
	 * current zone or adjacent zones are closer and available.
	 */
	if (oa > 0) {
		bool adjacentUATrigger = 0;
		bool adjacentOATrigger = 0;

		float minDist = 1000000;
		uint8_t minZoneIndex = 0;

		for (uint8_t i = 0; i < oa; i++) {
			float dist = distancetoPoint(ZONE_RESOURCE_POINT[OWNED_AVAILABLE[i]], getCurrentLocation());
			if (dist < minDist) {
				minDist = dist;
				minZoneIndex = i;
			}
		}

		uint8_t minoaZone = OWNED_AVAILABLE[minZoneIndex];

		dprintf("Closest resource zone: %d\n", minoaZone);

		//go to minoaZone, unless current zone or adjacent zone is closer and available
		if (zoneInArray(getZone(getCurrentLocation()), UNOWNED_AVAILABLE, ua) || zoneInArray((currentZone + 1) % 6, UNOWNED_AVAILABLE, ua)
				|| zoneInArray((currentZone - 1 + 6) % 6, UNOWNED_AVAILABLE, ua)) {
			adjacentUATrigger = 1;
			dprintf("AdjacentUATrigger is true\n");
		}
		if (zoneInArray(currentZone, OWNED_AVAILABLE, oa) || zoneInArray((currentZone + 1) % 6, OWNED_AVAILABLE, oa)
				|| zoneInArray((currentZone - 1 + 6) % 6, OWNED_AVAILABLE, oa)) {
			adjacentOATrigger = 1;
			dprintf("AdjacentOATrigger is true\n");
		}
		// No adjacent UA zones, go to nearest OA
		if (adjacentUATrigger == 0) {
			dprintf("No adjacent unowned available zones; going to mine nearest owned, available\n");
			*capturePoint = false;
			free(UNOWNED_AVAILABLE);
			free(UNOWNED_UNAVAILABLE);
			free(OWNED_AVAILABLE);
			free(OWNED_UNAVAILABLE);
			return minoaZone;
		} else { //if an adjacent is in UA, an adjacent still could be in OA, so check that first
			if (adjacentOATrigger == 0) {
				dprintf("There are OA zones, but an UA zone is closer\n");
				uint8_t returnZone = 0;
				if (zoneInArray(currentZone, UNOWNED_AVAILABLE, ua)) {
					*capturePoint = true;
					returnZone = currentZone;
				} else if (zoneInArray((currentZone + 1) % 6, UNOWNED_AVAILABLE, ua)) {
					*capturePoint = true;
					returnZone = (currentZone + 1) % 6;
				} else { //zoneInArray(getZone(getCurrentLocation) - 1...
					*capturePoint = true;
					returnZone = (currentZone + 6 - 1) % 6;
				}
				free(UNOWNED_AVAILABLE);
				free(UNOWNED_UNAVAILABLE);
				free(OWNED_AVAILABLE);
				free(OWNED_UNAVAILABLE);
				return returnZone;
			} else { //adjacentOATrigger == 1, so go directly to it instead.
				uint8_t returnZone;
				if (zoneInArray(currentZone, OWNED_AVAILABLE, oa)) {
					*capturePoint = false;
					returnZone = currentZone;
				} else if (zoneInArray((currentZone + 1) % 6, OWNED_AVAILABLE, oa)) {
					*capturePoint = false;
					returnZone = (currentZone + 1) % 6;
				} else { //if(zoneInArray(getZone(getCurrentLocation) -1, ...)
					*capturePoint = false;
					returnZone = (currentZone - 1 + 6) % 6;
				}
				free(UNOWNED_AVAILABLE);
				free(UNOWNED_UNAVAILABLE);
				free(OWNED_AVAILABLE);
				free(OWNED_UNAVAILABLE);
				return returnZone;
			}
		}

	} else { //NO ZONES CURRENTLY OWNED AND AVAILABLE --> first check UNOWNED_AVAILABLE
		if (ua > 0) { //if uncaptured but available, capture those first. mining them is first priority.
			int minuaZone;
			float minDist = 1000000;
			int minZoneIndex = 0;
			for (int zoneI = 0; zoneI < ua; zoneI++) {
				float dist = distancetoPoint(ZONE_CAPTURE_POINT[UNOWNED_AVAILABLE[zoneI]], getCurrentLocation());
				if (dist < minDist) {
					minDist = dist;
					minZoneIndex = zoneI;
				}
			}
			minuaZone = UNOWNED_AVAILABLE[minZoneIndex];
			dprintf("No OA zones; proceed to nearest UA zone\n");
			*capturePoint = true;
			free(UNOWNED_AVAILABLE);
			free(UNOWNED_UNAVAILABLE);
			free(OWNED_AVAILABLE);
			free(OWNED_UNAVAILABLE);
			return minuaZone;
		}
		//if none available, just capture those that aren't captured:
		//relevant zones to now check are unowned_unavailable.
		else {
			dprintf("No OA or UA zones; capture UU zones for points\n");
			int minuuZone;
			int minZoneIndex = 0;
			float minDist = 1000000;
			for (int zoneIndex = 0; zoneIndex < uu; zoneIndex++) {
				float dist = distancetoPoint(ZONE_RESOURCE_POINT[UNOWNED_UNAVAILABLE[zoneIndex]], getCurrentLocation());
				if (dist < minDist) {
					minDist = dist;
					minZoneIndex = zoneIndex;
				}
			}

			minuuZone = UNOWNED_UNAVAILABLE[minZoneIndex];
			*capturePoint = true;
			free(UNOWNED_AVAILABLE);
			free(UNOWNED_UNAVAILABLE);
			free(OWNED_AVAILABLE);
			free(OWNED_UNAVAILABLE);
			return minuuZone;
		}
	}
}

bool zoneInArray(uint8_t zone, uint8_t zoneArray[], uint8_t arrayLength) {
	for (uint8_t z = 0; z < arrayLength; z++) {
		if (zoneArray[z] == zone) {
			return true;
		}
	}
	return false;
}
