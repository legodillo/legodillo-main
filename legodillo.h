#ifndef LEGODILLO_H
#define LEGODILLO_H

#include "robot_physical_constants.h"
#include <math.h>

#define PI 3.14159265358979323846
#define GYRO_PORT 20
#define LSB_US_PER_DEG 1400000
#define TEAM_LEGODILLO 3

#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) > (b) ? (b) : (a))
#define constrain(a,min,max) ( (a) < (max) ? ((a) > (min) ? (a): (min)) : (max))

#define sign(a) ((a) > 0 ? 1 : ((a) < 0 ? -1 : 0))

// This was shamelessly copied from http://arduino.cc/en/Reference/Map
#define map(x,in_min, in_max, out_min, out_max) \
	(((x) - (in_min)) * ((out_max) - (out_min)) / ((in_max) - (in_min)) + (out_min))


#define dprintf(...)
#define dprint(...)

float normalizeHeading(float degrees);

inline float averageAnglesRad(float a1, float a2) {
	return atan2(sin(a1) + sin(a2), cos(a1) + cos(a2));
}

inline float averageAngles(float a1, float a2) {
	float a2Rad = a2 * PI / 180, a1Rad = a1 * PI / 180;
	return 180.0 / PI * averageAnglesRad(a1Rad, a2Rad);
}

#endif /* LEGODILLO_H */
