#ifndef ROBOT_PHYSICAL_CONSTANTS_H_
#define ROBOT_PHYSICAL_CONSTANTS_H_

// Distances are in cm
#define WHEEL_DIAMETER 8.50f //SEE BELOW
#define WHEEL_CIRCUMFERENCE 26.7f //NEEDS TO BE MEASURED MORE EXACTLY
#define WHEEL_SEPARATION  15.875f

#define TURRET_DIST_FROM_CENTER 6.0f // How far the center of the swivel is from the center of the robot
#define TURRET_DIST_SWIVEL_TO_LAUNCH 5.0f // How far the launch point is from the center of the swivel
#define WHEEL_GEAR_RATIO 75.0f
#define ENCODER_TICKS_PER_WHEEL_REV 75.0f
#define TURRET_HEADING_ENCODER_TO_DEGREES 360.f/336.f
#define TURRET_LAUNCHER_ENCODER_RATIO 30.f

#define ROBOT_FIDUCIAL_HEIGHT 28.0f // How high the fiducial pattern is off the ground
#define VPS_CAMERA_HEIGHT 400.0f // How high the VPS camera is off the ground
// Ports go here
#define MOTOR_LEFT_PORT 0
#define MOTOR_RIGHT_PORT 1
#define MOTOR_LAUNCHER_PORT 2
#define MOTOR_TURRET_PORT 3
#define MOTOR_CAPTURE_PORT 4

#define ENCODER_LEFT_PORT 24
#define ENCODER_RIGHT_PORT 25
#define ENCODER_LAUNCHER_PORT 26
#define ENCODER_TURRET_PORT 27

#define SERVO_LAUNCHER_PORT 0
#define SERVO_CAPTURE_PORT 2
#define SERVO_MINE_PORT 1

#define BREAKBEAM_SENSOR 19

//physical constants for turret RPS calculation below this line:
//________________________________________________________
#define DRAG_COEFF 0.47f
#define BALL_MASS 0.0025f
#define G 9.81f
#define THETA 40.2f
#define BALL_RADIUS 0.02f
#define WHEEL_RADIUS .0425f
#define RHO_AIR 1.2041f
#define ERROR_FACTOR 0.9245f
//_________________________________________________________

#endif /* ROBOT_PHYSICAL_CONSTANTS_H_ */
