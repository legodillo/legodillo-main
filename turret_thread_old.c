#include <math.h>
#include <joyos.h>
#include "legodillo.h"
#include "movement.h"
#include "robot_physical_constants.h"
#include "strategy_engine.h"
#include "rf_data_thread.h"

#define TURRET_SERVO_LAUNCHER_LAUNCH 325

#define TURRET_SERVO_LAUNCHER_NORMAL 247
#define TURRET_SERVO_LAUNCHER_LOAD 175
#define TURRET_LAUNCHER_SERVO_PORT 0
#define TURRET_LAUNCHER_MOTOR_PORT 2
#define TURRET_LAUNCHER_ENCODER_RATIO 30
#define TURRET_LAUNCHER_ENCODER_PORT 24
#define TURRET_HEADING_MOTOR_PORT 3
#define TURRET_HEADING_ENCODER_TO_DEGREES 360.0 / 336.0
#define TURRET_HEADING_ENCODER_PORT 25
#define TURRET_THREAD_PAUSE_TIME 100

#define TURRET_HEADING_TOLERANCE 2.5
#define TURRET_RPS_TOLERANCE .1

extern const struct location INNER_SAFE_NODES[6];
extern const struct location RED_SCORE_TARGET;
extern const struct location BLUE_SCORE_TARGET;

extern volatile bool CURRENT_COLOR;

// The last time any values were measured
uint32_t lastTime = 0;

// The last value read off the launcher encoder
uint16_t lastLauncherEncoderVal;

// The velocity the launcher encoder is set to
float launcherMotorVel = 0;

// How much more the launcher needs to be sped up in RPS
float launcherDeltaRPS;

// Target RPS for the launcher
volatile float launcherTargetRPS;

struct lock turretTargetLock;

struct location shootingFrom;

float shootingFromHeading;

bool targetChanged;

float getNecessaryRPS(float distance);

// Checks to see how far the launcher's speed is off, and passes that to the PID controller
void turret_rps_monitor() {
	uint32_t currentTime = get_time();
	uint16_t currentLauncherEncoderVal = encoder_read(TURRET_LAUNCHER_ENCODER_PORT);
	float encoderRPS = ((float) (currentLauncherEncoderVal - lastLauncherEncoderVal) / (currentTime - lastTime)) * 1000.;
	float wheelRPS = encoderRPS / TURRET_LAUNCHER_ENCODER_RATIO; //convert to wheel rotations per second
	launcherDeltaRPS = launcherTargetRPS - wheelRPS;
	dprintf("t_t: launcherDeltaRPS: %6.2f\n",launcherDeltaRPS);
	// Optionally, we could bias this towards slowing down by multiplying negative out values by a constant
	if (launcherTargetRPS == 0) {
		launcherMotorVel = 0;
		motor_set_vel(TURRET_LAUNCHER_MOTOR_PORT, -launcherMotorVel);
	} else {
		launcherMotorVel += launcherDeltaRPS;
		launcherMotorVel = constrain(launcherMotorVel, 0, 255);
		dprintf("t_t: launcherVel: %i\n",launcherMotorVel);
		motor_set_vel(TURRET_LAUNCHER_MOTOR_PORT, (int) -launcherMotorVel); // The motor spins backwards for positive values
	}

	lastLauncherEncoderVal = currentLauncherEncoderVal;

	// Because the encoder can spin around 20 * 30 = 600 times per second in a 120-second match,
	// it could actually reach the uint16 upper bound of 2^16 - 1 = 65,535
	// To prevent this, we reset the encoder whenever the motor is turned off by the PID controller
	if (launcherMotorVel == 0) {
		dprint("t_t: reseting launcher encoder\n");
		encoder_reset(TURRET_LAUNCHER_ENCODER_PORT);
		lastLauncherEncoderVal = 0;
	}

	lastTime = currentTime;
}

// Sets the desired launcher RPS
void setTurretRPS(float inputTargetRPS) {
	dprintf("t_t: setTurretRPS: %6.2f\n",inputTargetRPS);
	launcherTargetRPS = inputTargetRPS;
}

// Where to aim the turret, relative to pointing straight back. Positive values are counterclockwise
float targetRelativeAngle;

// Last encoder reading from the heading encoder
uint16_t lastHeadingEncoder;

// True if the turret is currently turning counterclockwise
bool turretTurningPositive = false;

// The absolute position of the turret, measured in encoder ticks from the center
int encoderTicksFromCenter = 0;

// How far the turret still needs to turn to reach its new heading
float deltaHeading;

// Notifies the thread when a new heading is set
volatile bool newHeadingSet = false;

// Checks to see how far the turret's heading is off, and passes that to the PID controller
void turret_heading_monitor() {
	uint16_t headingEncoderVal = encoder_read(TURRET_HEADING_ENCODER_PORT);
	encoderTicksFromCenter += (turretTurningPositive ? 1 : -1) * (headingEncoderVal - lastHeadingEncoder);
	deltaHeading = targetRelativeAngle - encoderTicksFromCenter * TURRET_HEADING_ENCODER_TO_DEGREES;
	dprintf("t_t: heading_monitor:deltaHeading: %6.2f\n",deltaHeading);
	if (abs(deltaHeading) >= TURRET_HEADING_TOLERANCE) {
		float motorOutput = sign(deltaHeading) * constrain( map(abs(deltaHeading),0,180,170,255),0,255);
		dprintf("t_t: motorOutput: %6.2f\n",motorOutput);
		// Motor is reversed
		motor_set_vel(TURRET_HEADING_MOTOR_PORT, -motorOutput);
	} else {
		motor_brake(TURRET_HEADING_MOTOR_PORT);
	}
	turretTurningPositive = deltaHeading > 0;
	lastHeadingEncoder = headingEncoderVal;
}

void turret_thread_setup() {
	init_lock(&turretTargetLock, "turret_target_lock");
	encoder_reset(TURRET_LAUNCHER_ENCODER_PORT);
	encoder_reset(TURRET_HEADING_ENCODER_PORT);
}

int turret_thread_main() {
	while (1) {
		acquire(&turretTargetLock);
		if (targetChanged) {
			dprint("t_t: TARGET CHANGED\n");
			struct location target = (CURRENT_COLOR == RED) ? RED_SCORE_TARGET : BLUE_SCORE_TARGET;
			struct location turretCenter = getOffsetLocation(shootingFrom, shootingFromHeading, -TURRET_DIST_FROM_CENTER);
			float headingToTarget = getHeadingToLocation(turretCenter, target);
			struct location turretLaunchPoint = getOffsetLocation(turretCenter, headingToTarget, TURRET_DIST_SWIVEL_TO_LAUNCH);
			targetRelativeAngle = normalizeHeading(headingToTarget - (gyro_get_degrees() + 180.0));
			launcherTargetRPS = getNecessaryRPS(distancetoPoint(turretLaunchPoint, target));
			targetChanged = false;
			dprintf("t_t: headingToTarget %6.2f; launcherAngle: %6.2f\n",targetRelativeAngle);
		}
		release(&turretTargetLock);
		//TODO turn this back on!
		turret_rps_monitor();
		turret_heading_monitor();
		pause(TURRET_THREAD_PAUSE_TIME);
	}
	return 0; // Not really
}

void turret_thread_start() {
	servo_set_pos(TURRET_LAUNCHER_SERVO_PORT, TURRET_SERVO_LAUNCHER_NORMAL);
	create_thread(&turret_thread_main, 10, 100, "turret_thread");
}

void turretAimAt(struct location shootFrom, float shootFromHeading) {
	acquire(&turretTargetLock);
	targetChanged = true;
	shootingFrom = shootFrom;
	shootingFromHeading = shootFromHeading;
	release(&turretTargetLock);
}

float getNecessaryRPS(float distToBallZone) {
	float anglerad = (THETA * (PI / 180.0));
	float numerator = (distToBallZone * ERROR_FACTOR / 100.0 * G);
	float denominator = (pow((WHEEL_RADIUS), 2) * sin(2 * anglerad)
			- (1 / (2 * BALL_MASS)) * DRAG_COEFF * RHO_AIR * PI * pow((BALL_RADIUS), 2) * pow((WHEEL_RADIUS), 2) * distToBallZone * ERROR_FACTOR / 100.0);
	float coeff = (1.0 / (2.0 * PI));
	float RPS = (coeff * sqrt((numerator) / (denominator)));
	return RPS;
}

bool turretIsReady() {
	return (abs(launcherDeltaRPS) < TURRET_RPS_TOLERANCE) && (abs(deltaHeading) < TURRET_HEADING_TOLERANCE);
}

void turretFire() {
	servo_set_pos(TURRET_LAUNCHER_SERVO_PORT, TURRET_SERVO_LAUNCHER_LOAD);
	pause(250);
	servo_set_pos(TURRET_LAUNCHER_SERVO_PORT, TURRET_SERVO_LAUNCHER_LAUNCH);
	pause(250);
	servo_set_pos(TURRET_LAUNCHER_SERVO_PORT, TURRET_SERVO_LAUNCHER_NORMAL);
	pause(250);
}
