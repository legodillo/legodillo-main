#include <joyos.h>
#include <math.h>
#include "legodillo.h"

float normalizeHeading(float degrees) {
	int floorangle = floor(degrees);
	float anglefrac = degrees - floorangle;
	float angle = floorangle % 360 + anglefrac;
	if (angle > 180) {
		return angle - 360; //if angle past 180, return value on [-180,0]
	} else if (angle < -180) {
		return angle + 360;
	} else
		return angle;
}
