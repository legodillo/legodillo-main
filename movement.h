#ifndef MOVEMENT_H_
#define MOVEMENT_H_

#include "legodillo.h"
#include "location.h"

#define MOTOR_LEFT 0
#define MOTOR_RIGHT 1
#define MOTOR_BOTH -1

#define PATH_CLOCKWISE 1
#define PATH_COUNTERCLOCKWISE 0

void setVelocity(int motor, int velocity);

void brake(int motor);

bool turnToHeading(float heading);

void goToHeading(struct location dest);

bool goToPoint(struct location dest);

void calculateCheckPoints(struct location start, struct location finish, uint8_t direction, struct location checkPoints[6],
		uint8_t* checkPointCountOut, uint8_t recursions);

bool goStraight(struct location dest);

void setLocationFromVPS();

struct location getCurrentLocation();

inline float getHeading(void) {
// wrapper function for returning gyro readout between [-180, 180] range.
	return normalizeHeading(gyro_get_degrees());
}

inline uint8_t getZone(struct location loc) {
	// + 360 is to get all positive angles,
	// + 30 is because zone 0's center is at theta = 0, not an edge
	// Each zone is 60 degrees
	return (uint8_t) ((360 + 30 + 180 / PI * atan2(loc.y, loc.x)) / 60) % 6;
}

#endif
//void movementStop();
//
//void forwardsTowardsPoint(struct location dest);
//
//void turnTowardsHeading(float turnTowards);
//void moveTowardsPoint(struct location dest);
//
//void updateLocation();
//
//void setLocationFromVPS();
//
//void turnTowardsHeading(float turnTowards);
//
//bool arrivedAtDestination(struct location destination);
//
//void calculateCheckPoints(struct location start, struct location finish, struct location checkPoints[3], uint8_t* checkPointCount);
//
//int getVelocity(int motor);
//
//void movementSetDestination(struct location destination, float finalHeading);
//
//
//
//float calculatePlannedDistance(struct location from, struct location to);
//
//void movement_thread_setup();
//
//void movement_thread_start();
//
//int movement_thread_main();
//
//#endif /* MOVEMENT_H_ */
