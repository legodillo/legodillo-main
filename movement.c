#include <joyos.h>
 #include "legodillo.h"
 #include "movement.h"
 #include "location.h"
 #include "strategy_engine.h"

 int velocityL, velocityR;

 extern const struct location INNER_SAFE_NODES[6];
 extern const struct location BOARD_CENTER;

float lastGyroHeading;
struct location currentLocation;

uint16_t lastEncoderLeft, lastEncoderRight;

 void setVelocity(int motor, int velocity);

 void brake(int motor);

 void updateLocation();

 int getVelocity(int motor) { //returns the velocity of any given motor
 	switch (motor) {
 	case MOTOR_LEFT:
 		return velocityL;
 	case MOTOR_RIGHT:
 		return velocityR;
 	default:
 		return 0;
 	}
 }

 void setVelocity(int motor, int velocity) { //sets the velocity for a motor and allows it to be retrieved with getVelocity
 	switch (motor) {
 	case MOTOR_LEFT:
 		velocityL = velocity;
 		motor_set_vel(MOTOR_LEFT_PORT, -velocity);
 		break;
 	case MOTOR_RIGHT:
 		velocityR = velocity;
 		motor_set_vel(MOTOR_RIGHT_PORT, -velocity);
 		break;
 	case MOTOR_BOTH:
 		velocityL = velocity;
 		velocityR = velocity;
 		motor_set_vel(MOTOR_LEFT_PORT, -velocity);
 		motor_set_vel(MOTOR_RIGHT_PORT, -velocity);
 		break;
 	}
 }

 void brake(int motor) {
 	switch (motor) {
 	case MOTOR_LEFT:
 		velocityL = 0;
 		motor_brake(MOTOR_LEFT_PORT);
 		break;
 	case MOTOR_RIGHT:
 		velocityR = 0;
 		motor_brake(MOTOR_RIGHT_PORT);
 		break;
 	case MOTOR_BOTH:
 		velocityL = velocityR = 0;
 		motor_brake(MOTOR_LEFT_PORT);
 		motor_brake(MOTOR_RIGHT_PORT);
 		break;
 	}
 }

bool turnToHeading(float absNewHeading) {
 	float deltaHeadingToDest = normalizeHeading(absNewHeading - getHeading());
 	dprintf("deltaHeading %6.2f \n", deltaHeadingToDest);
	int lastEncoder = 0;
	uint32_t lastEncoderBad = 0;
	uint32_t lastEncoderReadTime = get_time();

 	while (abs(deltaHeadingToDest) > 5) {
 		float motorSpeed = max(abs(deltaHeadingToDest) * .7,80) * sign(deltaHeadingToDest);

		uint32_t currentTime = get_time();

		if (((int) (lastEncoderLeft + lastEncoderRight) / 2 - lastEncoder) * 1000 < 5 * (currentTime - lastEncoderReadTime)) {
			if (lastEncoderBad == 0)
				lastEncoderBad = get_time();
		} else
			lastEncoderBad = 0;

 		setVelocity(MOTOR_LEFT, -motorSpeed);
 		setVelocity(MOTOR_RIGHT, motorSpeed);
 		deltaHeadingToDest = normalizeHeading(absNewHeading - getHeading());

		if (lastEncoderBad != 0 && getGameElapsedTime() > 10000 && currentTime - lastEncoderBad > 2000) {
			brake(MOTOR_BOTH);
			return false;
		}
		lastEncoder = (lastEncoderLeft + lastEncoderRight) / 2;
		lastEncoderLeft=encoder_read(ENCODER_LEFT_PORT);
		lastEncoderLeft=encoder_read(ENCODER_RIGHT_PORT);
		lastEncoderReadTime = get_time();

 	}
 	brake(MOTOR_BOTH);
 	dprintf("turning done\n");
	return true;
 }

bool goStraight(struct location dest) {
 	lastGyroHeading = getHeading();
 	encoder_reset(ENCODER_LEFT_PORT);
 	encoder_reset(ENCODER_RIGHT_PORT);
 	lastEncoderLeft = lastEncoderRight = 0;
 	float distance = distancetoPoint(currentLocation, dest);
	int lastEncoder = 0;
	uint32_t lastEncoderBad = 0;
	uint32_t lastEncoderReadTime = get_time();

	while (distance > 2) {
 		float headingToPoint = getHeadingToLocation(currentLocation, dest);
 		float deltaHeading = normalizeHeading(headingToPoint - getHeading());
 		int maxBaseSpeed = getGameElapsedTime() > 10000 ? 160 : 200;
		float baseSpeed = map(distance, 15., 25., 60.,maxBaseSpeed);
 		baseSpeed = constrain(baseSpeed, 60,maxBaseSpeed);
 		float turnFactor = constrain(deltaHeading * 3.0,-120,120);
 		if (abs(deltaHeading) < 180 / PI * asin(5 / distance)) {
 			turnFactor /= 2;
 			dprintf("turnFactor adjusted; allowable heading: %6.2f\n", 180 / PI * asin(5 / distance));
 		}

		uint32_t currentTime = get_time();
		if (((int) (lastEncoderLeft + lastEncoderRight) / 2 - lastEncoder) * 1000 < 3 * (currentTime - lastEncoderReadTime)) {
			if (lastEncoderBad == 0)
				lastEncoderBad = get_time();
		} else
			lastEncoderBad = 0;

 		//printf("hTP: %6.2f deltaH: %6.2f baseSpeed %6.2f turnFactor %6.2f\n", headingToPoint, deltaHeading, baseSpeed, turnFactor);

		updateLocation();
 		setVelocity(MOTOR_LEFT, constrain(baseSpeed - turnFactor,-255,255));
 		setVelocity(MOTOR_RIGHT, constrain(baseSpeed + turnFactor,-255,255));


		if (lastEncoderBad != 0 && getGameElapsedTime() > 10000 && currentTime - lastEncoderBad > 2000) {
			brake(MOTOR_BOTH);
			return false;
		}

		lastEncoder = (lastEncoderLeft + lastEncoderRight) / 2;
		lastEncoderReadTime = get_time();

 		distance = distancetoPoint(currentLocation, dest);
	}
 	brake(MOTOR_BOTH);
	return true;

 }
bool goToPoint(struct location dest) {
 	float destHeading = getHeadingToLocation(currentLocation, dest);
	int count = 0;
	bool okay = false;
	while (!okay) {
		okay = turnToHeading(destHeading);
		if (okay)
			okay &= goStraight(dest);
		if (okay) {
			break;
		}
		setVelocity(MOTOR_BOTH, -200);
		pause(200);
		brake(MOTOR_BOTH);
		pause(300);
		setLocationFromVPS();
		count++;
		if (count > 3)
			return false;
	}
	return true;
 }

 struct location getCurrentLocation() {
 	return currentLocation;
 }

 void setLocationFromVPS() {
 	dprintf("m: calibrating from VPS\n");
 	copy_objects();
 	currentLocation.x = game.coords[0].x * (1. - ROBOT_FIDUCIAL_HEIGHT / VPS_CAMERA_HEIGHT) * 121.92 / 2048;
 	currentLocation.y = game.coords[0].y * (1. - ROBOT_FIDUCIAL_HEIGHT / VPS_CAMERA_HEIGHT) * 121.92 / 2048;

 	gyro_set_degrees(game.coords[0].theta * 180.0 / 2048);

 	lastGyroHeading = gyro_get_degrees();
 	lastEncoderLeft = encoder_read(ENCODER_LEFT_PORT);
 	lastEncoderRight = encoder_read(ENCODER_RIGHT_PORT);
 }

 void updateLocation() {
 	double currentHeading = getHeading();
 	double dTheta = normalizeHeading(currentHeading - lastGyroHeading);
 	double dThetaRad = dTheta * PI / 180;
 	int encoderLeft = encoder_read(ENCODER_LEFT_PORT);
 	int encoderRight = encoder_read(ENCODER_RIGHT_PORT);
 	int dLeftInt = (encoderLeft - lastEncoderLeft);
 	int dRightInt = (encoderRight - lastEncoderRight);
 	dprintf("dLeftInt: %d dRightInt %d\n",dLeftInt,dRightInt);
 	double dLeft = dLeftInt / ENCODER_TICKS_PER_WHEEL_REV * WHEEL_CIRCUMFERENCE;
 	double dRight = dRightInt / ENCODER_TICKS_PER_WHEEL_REV * WHEEL_CIRCUMFERENCE;
 	dprintf("dLeft: %6.2f dRight: %6.2f\n",dLeft, dRight);
 	dLeft = dLeft * sign(getVelocity(MOTOR_LEFT));
 	dRight = dRight * sign(getVelocity(MOTOR_LEFT));

 	double dX, dY;
 	dprintf("m: heading: %6.2f\n", currentHeading);
 	if (dLeft == 0 && dRight == 0) {
 		dX = dY = 0;
 		dprintf("m: dX = dY\n");
 	} else // Case where neither wheel is at zero
 	if ((dLeft != 0) && (dRight != 0)) {
 		// The wheels are going in the same direction
 		if (sign(dLeft) == sign(dRight)) {
 			dprintf("m: signLeft = sightRight\n");
 			if (dLeft == dRight) {
 				dprintf("dLeft = dRight\n");
 				dX = cos(PI / 180 * lastGyroHeading) * (dLeft + dRight) / 2;
 				dY = sin(PI / 180 * lastGyroHeading) * (dLeft + dRight) / 2;
 			} else {
 				double r, currentHeadingAdjustedRad, lastHeadingAdjustedRad;
 				if (abs(dLeft) > abs(dRight)) {
 					dprintf("m: abs dLeft > abs dRight\n");
 					r = -dRight / dThetaRad;
 					currentHeadingAdjustedRad = (currentHeading + 90) * PI / 180;
 					lastHeadingAdjustedRad = (lastGyroHeading + 90) * PI / 180;
 				} else // if (abs(dLeft)<abs(dRight))
 				{
 					dprintf("m: abs dLeft > abs dRight\n");
 					r = dLeft / dThetaRad;
 					currentHeadingAdjustedRad = (currentHeading - 90) * PI / 180;
 					lastHeadingAdjustedRad = (lastGyroHeading - 90) * PI / 180;
 				}
 				dX = (r + WHEEL_SEPARATION / 2) * (cos(currentHeadingAdjustedRad) - cos(lastHeadingAdjustedRad));
 				dY = (r + WHEEL_SEPARATION / 2) * (sin(currentHeadingAdjustedRad) - sin(lastHeadingAdjustedRad));
 			}
 		} else // The wheels are going in opposite directions
 		{
 			dprintf("m: sign Left != sign Right\n");
 			if (dLeft == -dRight) {
 				dprintf("dLeft = -dRight\n");
 				dX = dY = 0;
 			} else {
 				double r, currentHeadingAdjustedRad, lastHeadingAdjustedRad;
 				if (abs(dLeft) > abs(dRight)) {
 					dprintf("m: abs dLeft > abs dRight\n");
 					r = -dLeft / dThetaRad;
 					currentHeadingAdjustedRad = (currentHeading + 90) * PI / 180;
 					lastHeadingAdjustedRad = (lastGyroHeading + 90) * PI / 180;
 				} else //if (abs(dLeft)<abs(dRight))
 				{
 					dprintf("m: abs dLeft < abs dRight\n");
 					r = dRight / dThetaRad;
 					currentHeadingAdjustedRad = (currentHeading - 90) * PI / 180;
 					lastHeadingAdjustedRad = (lastGyroHeading - 90) * PI / 180;
 				}
 				dX = (r - WHEEL_SEPARATION / 2) * (cos(currentHeadingAdjustedRad) - cos(lastHeadingAdjustedRad));
 				dY = (r - WHEEL_SEPARATION / 2) * (sin(currentHeadingAdjustedRad) - sin(lastHeadingAdjustedRad));
 			}
 		}
 	} else // if (dLeft!=0 || dRight !=0)
 	{
 		dprintf("m: dLeft = 0 || dRight = 0\n");
 		dX = (WHEEL_SEPARATION / 2) * (cos(currentHeading * PI / 180) - cos(lastGyroHeading * PI / 180));
 		dY = (WHEEL_SEPARATION / 2) * (sin(currentHeading * PI / 180) - sin(lastGyroHeading * PI / 180));
 	}
 	dX = constrain(dX, -10,10);
 	dY = constrain(dY, -10,10);
 	dprintf("m: dX = %6.2f, dY = %6.2f\n", dX, dY);
 	currentLocation.x += dX;
 	currentLocation.y += dY;
 	dprintf("m: loc: x = %6.2f, y = %6.2f\n", currentLocation.x, currentLocation.y);
 	lastGyroHeading = currentHeading;
 	lastEncoderLeft = encoderLeft;
 	lastEncoderRight = encoderRight;
 }
