#ifndef LOCATION_MATH_H_
#define LOCATION_MATH_H_
#include <stdbool.h>
#include "legodillo.h"
#include "location.h"

//Define a location type to be used anywhere useful
struct location {
	float x; // The meaning of the fields should be fairly obvious :)
	float y;
};

float distancetoPoint(struct location from, struct location to);

float getHeadingToLocation(struct location from, struct location to);

bool lineSegmentIntersection(struct location A, struct location B, struct location C, struct location D, float* X, float* Y);

struct location getOffsetLocation(struct location input, float angle, float distance);

#endif /* LOCATION_H_ */
