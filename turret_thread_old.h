#ifndef TURRET_THREAD_H_
#define TURRET_THREAD_H_
#include <joyos.h>
#include "legodillo.h"
#include "movement.h"
#include "robot_physical_constants.h"

float turret_rps_pid_input();

void turret_rps_pid_output(float out);

void turret_thread_setup();

int turret_thread_main();

void turret_rps_monitor();

void turret_heading_monitor();

void setTurretRPS(float inputTargetRPS);

void turret_thread_start();

void turretAimAt(struct location shootFrom, float shootFromHeading);

float getNecessaryRPS(float distToBallZone);

bool turretIsReady();

void turretFire();

#endif
